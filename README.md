# Stark bank Invoice Webhook

## Running the application

To run the application, make sure you have Docker running and run the following command:

`$ docker-compose up --build`

## Configuration

Copy the `.env.example` file into a `.env` file and set up the following variables:

 - `PROJECT_ID` with your Starkbank Project ID
 - `PRIVATE_KEY` with the contents of your private key file


## Endpoints:

### Invoice creation

`/create_invoices`

Accepts: `POST`

Example call using (HTTPie)[https://httpie.io/]
`$ http POST localhost:8000/create_invoices`

Schedules 8 invoices for random people for the next 24 hours. Call with a `POST` method with no data

### Invoice Webhook

`/sb_webhook`

Accepts: `POST`

Accepts callbacks from the `invoices` subscription from the Starkbank API. If the invoice is paid, it schedules
a Celery call to make a transfer to a test account.

## Running locally and testing

### Config

To run the project locally, set up your `virtualenv` and export the environment variables above. I personally use [direnv](https://direnv.net/).
Example `.envrc` file:

``` .envrc
layout python3
export PROJECT_ID=123
export PRIVATE_KEY="BEGIN
data
END"
```
### Install dependencies

`pip install -r project/requirements.txt`

### Run unit tests

`pytest`


## Improvements:

- [ ] Integration tests
