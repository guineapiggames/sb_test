# project/app/tasks.py

import os

import celery

from app.utils.transfer_manager import transfer_to_test_account

CELERY_BROKER = os.environ.get("CELERY_BROKER")
CELERY_BACKEND = os.environ.get("CELERY_BACKEND")

app = celery.Celery("tasks", broker=CELERY_BROKER, backend=CELERY_BACKEND)


@app.task
def transfer_to_test_account_task(amount: int, fee: int):
    transfer_to_test_account(amount=amount, fee=fee)
