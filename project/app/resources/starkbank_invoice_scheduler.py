import falcon

from app.utils.invoice_manager import build_random_invoices, send_invoices


class StarkbankInvoiceScheduler:
    def on_post(self, req, resp):
        invoices = build_random_invoices()
        send_invoices(invoices)
        resp.status_code = falcon.HTTP_201
