import falcon

from app.tasks import transfer_to_test_account_task


class StarkbankWebhook:
    def on_post(self, req, resp):
        try:
            invoice_data = req.media["event"]["log"]
        except KeyError:
            resp.status = falcon.HTTP_BAD_REQUEST
            return
        if invoice_data.get("type", "") == "credited":
            transfer_to_test_account_task.delay(
                amount=int(invoice_data["invoice"]["amount"]),
                fee=int(invoice_data["invoice"]["fee"]),
            )
            resp.text = "Transfer scheduled"
        else:
            resp.text = "Invoice data received."
        resp.status = falcon.HTTP_OK
