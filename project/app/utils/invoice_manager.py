from datetime import datetime, timedelta

from more_itertools import batched
from starkbank import Invoice
from starkbank.invoice import create as create_invoice

from app.utils.random import RandomData
from app.utils.sdk import get_user


def _build_random_invoice(hours_from_now: int):
    random_data = RandomData()
    person = random_data.person()
    invoice = Invoice(
        amount=random_data.amount(),
        name=person["name"],
        tax_id=person["tax_id"],
        due=datetime.utcnow() + timedelta(hours=hours_from_now),
        fine=5,  # 5%
        interest=2.5,  # 2.5% per month
        tags=["scheduled"],
    )
    return invoice


def send_invoices(invoices):
    for invoice_list in batched(invoices, 100):
        create_invoice(invoice_list, user=get_user())


def build_random_invoices():
    invoices = []
    for hours in range(1, 25):
        for _ in range(0, 8):
            invoices.append(_build_random_invoice(hours))
    return invoices
