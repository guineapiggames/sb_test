import random

import names


class RandomData:
    PEOPLE = [
        {"name": names.get_full_name(), "tax_id": "124.735.837-22"},
        {"name": names.get_full_name(), "tax_id": "071.767.497-52"},
        {"name": names.get_full_name(), "tax_id": "150.614.118-89"},
        {"name": names.get_full_name(), "tax_id": "671.124.872-42"},
        {"name": names.get_full_name(), "tax_id": "364.888.651-70"},
        {"name": names.get_full_name(), "tax_id": "82.048.316/0001-18"},
        {"name": names.get_full_name(), "tax_id": "12.800.297/0001-59"},
        {"name": names.get_full_name(), "tax_id": "56.913.989/0001-89"},
        {"name": names.get_full_name(), "tax_id": "14.219.370/0001-29"},
        {"name": names.get_full_name(), "tax_id": "24.684.332/0001-09"},
    ]

    def amount(self) -> int:
        return random.randint(100, 40000)

    def person(self) -> dict[str, str]:
        return random.choice(self.PEOPLE)
