from starkbank import Transfer
from starkbank.transaction import create as create_transaction

from app.utils.sdk import get_user

TEST_ACCOUNT = {
    "bank_code": "20018183",
    "branch_code": "0001",
    "account_number": "6341320293482496",
    "name": "Stark Bank S.A.",
    "tax_id": "20.018.183/0001-80",
    "account_type": "payment",
}


def _build_transfer(total_amount, fee):
    total_amount = max([0, total_amount - fee])
    return Transfer(**TEST_ACCOUNT, amount=total_amount)


def transfer_to_test_account(amount: int, fee: int):
    create_transaction([_build_transfer(amount, fee)], user=get_user())
