import os

import starkbank

def get_user():
    return starkbank.Project(
        environment="sandbox", id=os.getenv('PROJECT_ID'), private_key=os.getenv('PRIVATE_KEY')
    )
