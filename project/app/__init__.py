import falcon

from app.resources.starkbank_invoice_scheduler import StarkbankInvoiceScheduler
from app.resources.starkbank_webhook import StarkbankWebhook

app = falcon.App()
app.add_route("/sb_webhook", StarkbankWebhook())
app.add_route("/create_invoices", StarkbankInvoiceScheduler())
