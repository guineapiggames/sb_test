from app.utils.transfer_manager import (
    TEST_ACCOUNT,
    _build_transfer,
    transfer_to_test_account,
)


def test_build_transfer_does_not_build_negative_amounts():
    transfer = _build_transfer(200, 300)
    assert transfer.amount == 0


def test_build_transfer_returns_the_correct_data():
    transfer = _build_transfer(200, 5)
    assert transfer.amount == 195
    assert transfer.bank_code == TEST_ACCOUNT["bank_code"]
    assert transfer.branch_code == TEST_ACCOUNT["branch_code"]
    assert transfer.account_number == TEST_ACCOUNT["account_number"]
    assert transfer.name == TEST_ACCOUNT["name"]
    assert transfer.tax_id == TEST_ACCOUNT["tax_id"]
    assert transfer.account_type == TEST_ACCOUNT["account_type"]


def test_transfer_to_test_account_calls_the_api(mocker):
    mocker.patch("app.utils.transfer_manager.get_user")
    mock_create_transaction = mocker.patch(
        "app.utils.transfer_manager.create_transaction"
    )
    transfer_to_test_account(200, 0)
    mock_create_transaction.assert_called_once()
