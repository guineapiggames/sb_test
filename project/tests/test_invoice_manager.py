from datetime import datetime
from random import randint

from app.utils.invoice_manager import (
    _build_random_invoice,
    build_random_invoices,
    send_invoices,
)
from app.utils.random import RandomData


def test_build_invoice_calls_the_api(mocker):
    mocker.patch("app.utils.invoice_manager.get_user")
    mock_create_invoice = mocker.patch("app.utils.invoice_manager.create_invoice")
    send_invoices([mocker.Mock()])
    mock_create_invoice.assert_called()


def test_build_invoice_bundles_the_calls_in_batches_of_100(mocker):
    mocker.patch("app.utils.invoice_manager.get_user")
    mock_create_invoice = mocker.patch("app.utils.invoice_manager.create_invoice")
    total_invoices = randint(1, 301)
    invoices = total_invoices * [mocker.Mock()]
    send_invoices(invoices)
    assert (
        mock_create_invoice.call_count == total_invoices / 100
        if (total_invoices % 100) == 0
        else total_invoices / 100 + 1
    )


def test_build_random_invoices_creates_8_invoices_per_hour_for_the_next_24_hours():
    invoices = build_random_invoices()
    assert len(invoices) == 8 * 24


def test_build_random_invoice_is_built_with_the_correct_data(mocker):
    mocker.patch.object(RandomData, "amount").return_value = 666
    mocker.patch.object(RandomData, "person").return_value = {
        "name": "Eddie",
        "tax_id": "17.41",
    }
    mocker.patch("app.utils.invoice_manager.datetime").utcnow.return_value = datetime(
        1984, 8, 6, 23, 58
    )

    invoice = _build_random_invoice(hours_from_now=2)

    assert invoice.amount == 666
    assert invoice.name == "Eddie"
    assert invoice.tax_id == "17.41"
    assert invoice.due == datetime(1984, 8, 7, 1, 58)
    assert invoice.fine == 5
    assert invoice.interest == 2.5
    assert invoice.tags == ["scheduled"]
